<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<script src="js/bootstrap.min.js"></script>

		<meta charset="ISO-8859-1">
		<title>Rock Paper Scissors Game</title>
	</head>

	<body>
		<div class="container">
			<h2>Rock Paper Scissors</h2>
			<h3>Round X</h3>
			<!--Results list-->
			<form action="/games-app" method="post" id="gameForm" role="form">
				<c:choose>
					<c:when test="${not empty resultsList}">
						<table class="table table-striped">
							<thead>
								<tr>
									<td>Round</td>
									<td>Player 1 choice</td>
									<td>CPU choice</td>
									<td>Score</td>
								</tr>
							</thead>
							<c:forEach var="result" items="${resultsList}">
								<tr>
									<td>${result.round}</td>
									<td>${result.choiceA}</td>
									<td>${result.choiceB}</td>
									<td>${result.result}</td>
								</tr>
							</c:forEach>
						</table>
					</c:when>
				</c:choose>
			</form>
			<!-- Player controls-->
			<form action="/games-app" method="get" id="playerControl" role="form">
				<input type="hidden" id="playerController" name="actionsController" value="newMove">
				<div class="form-group col-xs-5">
					<select name="playerChoice" id="playerChoice">
						<option value="ROCK">Rock</option>
						<option value="PAPER">Paper</option>
						<option value="SCISSORS">Scissors</option>
					</select>
				</div>
				<button type="submit" class="btn btn-info">Submit</button>
				<br></br>
                <br></br>
			</form>
			<!-- Restart button -->
			<form action="/games-app" method="get" id="restart" role="form">
				<input type="hidden" id="restartController" name="actionsController" value="restart">
				<button type="submit" class="btn btn-info">Submit</button>
				<br></br>
                <br></br>
			</form>
			<!-- Score -->
			<form action="/games-app" method="post" id="gameForm" role="form">
				<c:choose>
					<c:when test="${not empty score}">
						<table class="table table-striped">
							<thead>
								<tr>
									<td>Rounds</td>
									<td>Victories</td>
									<td>Draws</td>
									<td>Loses</td>
								</tr>
							</thead>
							<c:forEach var="result" items="${score}">
								<tr>
									<td>${result.rounds}</td>
									<td>${result.victories}</td>
									<td>${result.draws}</td>
									<td>${result.loses}</td>
								</tr>
							</c:forEach>
						</table>
					</c:when>
				</c:choose>
			</form>
		</div>	
	</body>

</html>