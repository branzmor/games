package com.example.games;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.games.enums.Choice;
import com.example.players.Score;

@WebServlet(name = "RockPaperScissorsServlet", urlPatterns = { "/games-app" })
public class RockPaperScissorsServlet extends HttpServlet {

	RockPaperScissorsService service = new RockPaperScissorsService();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String action = req.getParameter("actionsController");
		if(action != null) {
			switch (action) {
				case "restart":
				restart(req,resp);
				break;
				case "newMove":
				newMovement(req,resp);
				break;
			}
		}else {
			List<RockPaperScissorsResult> resultsList = service.getResults();
			req.setAttribute("resultList",resultsList);
			Score score = service.getScore();
			req.setAttribute("score",score);
		}
	}

	private void restart(HttpServletRequest req, HttpServletResponse resp){
		try{
			service.restartGame();
		}catch(Exception e){
			Logger.getLogger(RockPaperScissorsServlet.class.getName()).log(Level.SEVERE, null, e);
		}
	}

	private void newMovement(HttpServletRequest req, HttpServletResponse resp){
		try {
			Choice playerChoice = (Choice) req.getAttribute("playerChoice");
			service.getGame().setHand(playerChoice,Choice.getRandomChoice());
			service.getGame().setRound(service.getGame().getRound()+1);
		} catch (Exception e) {
			Logger.getLogger(RockPaperScissorsServlet.class.getName()).log(Level.SEVERE, null, e);
		}
	}
}
