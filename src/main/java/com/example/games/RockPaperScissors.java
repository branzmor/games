package com.example.games;

import com.example.games.enums.Result;
import com.example.players.Player;

import java.util.List;

import com.example.games.enums.Choice;

public class RockPaperScissors implements Game {
	
	private Player player;
	private int round = 0;
	private List<RockPaperScissorsResult> results;
	private RockPaperScissorsResult currentResult = null;
	
	/**
	 * 
	 * @param player
	 */
	public RockPaperScissors(Player player) {
		this.player = player;
	}
	
	public RockPaperScissorsResult getCurrentResult() {
		return currentResult;
	}
	
	@Override
	public void newGameSinglePlayer(Player player) {
		this.setPlayer(player);
		this.round++;
		setHand(player.getChoice(), Choice.getRandomChoice());
	}
	
	
	@Override
	public void restart() {
		this.round = 0;
		newGameSinglePlayer(player);
	}
	
	/**
	 * 
	 * @param choiceA
	 * @param choiceB
	 */
	public void setHand(Choice choiceA, Choice choiceB) {
		
		Result score = validateChoices(choiceA, choiceB);
		currentResult = new RockPaperScissorsResult(choiceA,choiceB,this.round,score);
		results.add(currentResult);
	}
	
	/**
	 * 
	 * @param choice1
	 * @param choice2
	 * @return true player1 wins, false player2 wins
	 */
	private Result validateChoices(Choice choice1, Choice choice2) {
		if(choice1 == choice2) {
			return Result.DRAW;
		}else{
			if(choice1 == Choice.ROCK) {
				if(choice2 == Choice.SCISSORS) {
					return Result.VICTORY;
				}else {
					return Result.LOST;
				}
			}else if(choice1 == Choice.PAPER) {
				if(choice2 == Choice.ROCK) {
					return Result.VICTORY;
				}else {
					return Result.LOST;
				}
			}else {
				if(choice2 == Choice.PAPER) {
					return Result.VICTORY;
				}else {
					return Result.LOST;
				}
			}
		}
	}


	public Player getPlayer() {
		return player;
	}


	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public int getRound() {
		return round;
	}


	public void setRound(int round) {
		this.round = round;
	}
	
	public List<RockPaperScissorsResult> getResults(){
		return results;
	}
	
	public void setResults(List<RockPaperScissorsResult> results) {
		this.results = results;
	}
}
