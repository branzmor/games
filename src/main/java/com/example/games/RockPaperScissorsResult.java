package com.example.games;

import com.example.games.enums.Choice;
import com.example.games.enums.Result;

public class RockPaperScissorsResult {
	
	private final Choice choiceA;
	private final Choice choiceB;
	private int round;
	private final Result result;
	
	public RockPaperScissorsResult(	final Choice choiceA,
									final Choice choiceB,
									final int round,
									final Result result) {
		this.choiceA = choiceA;
		this.choiceB = choiceB;
		this.round = round;
		this.result = result;
	}
	
	public Choice getChoiceA() {
		return choiceA;
	}
	
	public Choice getChoiceB() {
		return choiceB;
	}
	
	public int getRound() {
		return round;
	}
	
	// Always be player one result until second player option in the future
	public Result getResult() {
		return result;
	}	
}
