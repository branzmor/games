package com.example.games.enums;

import java.util.Random;

public enum Choice {
	ROCK,
	PAPER,
	SCISSORS;
	
	/**
     * Pick a random value of the Choice enum.
     * @return a random Choice.
     */
    public static Choice getRandomChoice() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
}
