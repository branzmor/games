package com.example.games.enums;

public enum Result {
	VICTORY,
	LOST,
	DRAW
}
