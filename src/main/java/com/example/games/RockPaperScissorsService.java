package com.example.games;

import java.util.List;

import com.example.players.Player;
import com.example.players.PlayerList;
import com.example.players.Score;

public class RockPaperScissorsService {
	
	RockPaperScissors game = null;
	Score score = Player.getScore();
	
	public RockPaperScissorsService() {
		Player player = new Player();
		this.game = new RockPaperScissors(player);
	}
		
	public List<RockPaperScissorsResult> getResults(){
		return game.getResults();
	}
	
	public Score getScore(){
		return score;
	}
	
	public void restartGame() {
		game.restart();
	}
	
	public RockPaperScissors getGame() {
		return this.game;
	}
}
