package com.example.games;

import com.example.players.Player;

public interface Game {

	public void newGameSinglePlayer(Player player);
	
	public void restart();
	
	//TODO Future idea
	//public void newGameMultiplayer(Player playerA, Player playerB);
	
}
