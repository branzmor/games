package com.example.players;

import com.example.games.enums.Choice;
import com.example.games.enums.Result;
import com.example.players.Score;

public class Player {
	
	private Choice choice;
	private int points = 0;
	private static Score score;
	
	public Player() {
		this.score = new Score();
	}
	
	public Choice getChoice() {
		return this.choice;
	}
	
	public void setChoice(Choice choice) {
		this.choice = choice;
	}
	
	public int getPoints() {
		return this.points;
	}
	
	public void increasePoints() {
		this.points++;
	}
	
	public void updateScore(Result result) {
		score.updateScore(result);
	}
	
	public void clearScore() {
		this.points = 0;
	}
	
	public static Score getScore() {
		return score;
	}
}
