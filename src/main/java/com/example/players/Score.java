package com.example.players;

import com.example.games.enums.Result;

public class Score {
	
	private int rounds;
	private int victories;
	private int draws;
	private int loses;
	
	public Score() {
		this.rounds = 0;
		this.victories = 0;
		this.loses = 0;
		this.draws = 0;
	}
	
	public int getRounds() {
		return rounds;
	}
	public void setRounds(int rounds) {
		this.rounds = rounds;
	}
	public int getVictories() {
		return victories;
	}
	public void setVictories(int victories) {
		this.victories = victories;
	}
	public int getDraws() {
		return draws;
	}
	public void setDraws(int draws) {
		this.draws = draws;
	}
	public int getLoses() {
		return loses;
	}
	public void setLoses(int loses) {
		this.loses = loses;
	}
	
	
	public void updateScore(Result result) {
		if(result == Result.VICTORY) {
			setVictories(getVictories() + 1);
		} else if(result == Result.DRAW) {
			setDraws(getDraws() + 1);
		}else if(result == Result.LOST) {
			setLoses(getLoses() + 1);
		}
		
		setRounds(getRounds() + 1);
	}
	
}
