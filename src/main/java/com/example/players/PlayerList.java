package com.example.players;

import java.util.ArrayList;
import java.util.List;

import com.example.games.enums.Choice;

public class PlayerList {
	
	private static final List<Player> playerList = new ArrayList();
	
	private PlayerList() {
		playerList.add(new Player());
		playerList.add(new Player());
		playerList.add(new Player());
		
	}

	public static List<Player> getInstance() {
		return playerList;
	}

}
