package com.example.game;

import com.example.games.RockPaperScissors;
import com.example.games.RockPaperScissorsResult;
import com.example.players.Player;

public class GameEngine {
	
	public  RockPaperScissorsResult play(final Player playerA,final Player playerB) {
		RockPaperScissors game = new RockPaperScissors(playerA);
		game.setHand(playerA.getChoice(), playerB.getChoice());
		return game.getCurrentResult();	
	}
}
