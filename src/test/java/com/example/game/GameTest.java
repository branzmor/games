package com.example.game;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.example.games.RockPaperScissors;
import com.example.games.RockPaperScissorsResult;
import com.example.games.enums.Choice;
import com.example.games.enums.Result;
import com.example.players.Player;

public class GameTest {
	
	private static final RockPaperScissorsResult run(final Player playerA, final Player playerB) {
		final GameEngine gameEngine = new GameEngine();
		return gameEngine.play(playerA,playerB);
	}
	
	@Test
	public void test1() {
		final Player playerA = new Player();
		final Player playerB = new Player();
		assertNotNull(playerA);
		assertNotNull(playerB);
		playerA.setChoice(Choice.ROCK);
		playerB.setChoice(Choice.SCISSORS);
		final RockPaperScissorsResult gameResult = run(playerA, playerB);
		assertNotNull(gameResult);
		assertThat(gameResult.getChoiceA(),is(Choice.ROCK));
		assertThat(gameResult.getChoiceB(),is(Choice.SCISSORS));
		assertThat(gameResult.getResult(),is(Result.VICTORY));
	}
	
	@Test
	public void test2() {
		final Player playerA = new Player();
		final Player playerB = new Player();
		assertNotNull(playerA);
		assertNotNull(playerB);
		playerA.setChoice(Choice.PAPER);
		playerB.setChoice(Choice.ROCK);
		final RockPaperScissorsResult gameResult = run(playerA, playerB);
		assertNotNull(gameResult);
		assertThat(gameResult.getChoiceA(),is(Choice.PAPER));
		assertThat(gameResult.getChoiceB(),is(Choice.ROCK));
		assertThat(gameResult.getResult(),is(Result.VICTORY));
	}
	
	@Test
	public void test3() {
		final Player playerA = new Player();
		final Player playerB = new Player();
		assertNotNull(playerA);
		assertNotNull(playerB);
		playerA.setChoice(Choice.SCISSORS);
		playerB.setChoice(Choice.PAPER);
		final RockPaperScissorsResult gameResult = run(playerA, playerB);
		assertNotNull(gameResult);
		assertThat(gameResult.getChoiceA(),is(Choice.SCISSORS));
		assertThat(gameResult.getChoiceB(),is(Choice.PAPER));
		assertThat(gameResult.getResult(),is(Result.VICTORY));
	}
	
	@Test
	public void test4() {
		final Player playerA = new Player();
		final Player playerB = new Player();
		assertNotNull(playerA);
		assertNotNull(playerB);
		playerA.setChoice(Choice.ROCK);
		playerB.setChoice(Choice.PAPER);
		final RockPaperScissorsResult gameResult = run(playerA, playerB);
		assertNotNull(gameResult);
		assertThat(gameResult.getChoiceA(),is(Choice.ROCK));
		assertThat(gameResult.getChoiceB(),is(Choice.PAPER));
		assertThat(gameResult.getResult(),is(Result.LOST));
	}
	
	@Test
	public void test5() {
		final Player playerA = new Player();
		final Player playerB = new Player();
		assertNotNull(playerA);
		assertNotNull(playerB);
		playerA.setChoice(Choice.PAPER);
		playerB.setChoice(Choice.PAPER);
		final RockPaperScissorsResult gameResult = run(playerA, playerB);
		assertNotNull(gameResult);
		assertThat(gameResult.getChoiceA(),is(Choice.PAPER));
		assertThat(gameResult.getChoiceB(),is(Choice.PAPER));
		assertThat(gameResult.getResult(),is(Result.DRAW));
	}
}
